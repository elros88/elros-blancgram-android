package com.blanclink.blancgram.imageContainer;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.blanclink.blancgram.R;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by danort on 8/7/16.
 */
public class ImageContainer
{
    ImageView imageView;
    Context context;

    public ImageContainer(Context context, ImageView imageView)
    {
        this.context = context;
        this.imageView = imageView;
    }

    public void saveImage()
    {
        Bitmap source = null;
        source = ((BitmapDrawable)imageView.getDrawable()).getBitmap();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String currentDateandTime = sdf.format(new Date());

        String imagePath = MediaStore.Images.Media.insertImage(
                context.getContentResolver(), source,
                context.getString(R.string.app_name) + currentDateandTime,
                context.getString(R.string.app_name) + currentDateandTime
        );

        Uri path = Uri.parse(imagePath);

        Toast.makeText(context, context.getString(R.string.save_image_mesage), Toast.LENGTH_LONG).show();

    }
}
