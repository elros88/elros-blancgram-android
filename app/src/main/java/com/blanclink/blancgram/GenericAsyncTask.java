package com.blanclink.blancgram;

import android.os.AsyncTask;

/**
 * Created by danort on 8/6/16.
 */
public abstract class GenericAsyncTask <Params, Progress, Result> extends AsyncTask <Params, Progress, Result>
{
    @Override
    protected void onPostExecute(Result result) {
        super.onPostExecute(result);
        onDone(result);
    }

    public abstract void onDone(Result result);
}
