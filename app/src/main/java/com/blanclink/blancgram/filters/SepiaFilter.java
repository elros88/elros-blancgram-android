package com.blanclink.blancgram.filters;

import android.graphics.Bitmap;
import android.widget.ImageView;

/**
 * Created by danort on 8/6/16.
 */
public class SepiaFilter extends Filter
{

    public SepiaFilter(ImageView imageView)
    {
        super(imageView);
    }

    @Override
    public Bitmap applyFilter() {

        configureFilter();

        if (originalPixels != null) {
            source.getPixels(originalPixels, 0, source.getWidth(), 0, 0, source.getWidth(), source.getHeight());
            for (int i = 0; i < originalPixels.length; i++) {
                int alpha = getAlphaFromPixel(originalPixels[i]);
                int red = getRedFromPixel(originalPixels[i]);
                int green = getGreenFromPixel(originalPixels[i]);
                int blue = getBlueFromPixel(originalPixels[i]);
                int newPixel = createPixel(alpha,
                        (int)Math.min(255, (red * 0.393) + (green * 0.769) + (blue * 0.189)),
                        (int)Math.min(255, (red * 0.349) + (green * 0.686) + (blue * 0.168)),
                        (int)Math.min(255, (red * 0.272) + (green * 0.534) + (blue * 0.131)));
                originalPixels[i] = newPixel;
            }
            resultingBitmap =  Bitmap.createBitmap(originalPixels, source.getWidth(), source.getHeight(), Bitmap.Config.ARGB_8888);
        }
        return resultingBitmap;
    }
}
