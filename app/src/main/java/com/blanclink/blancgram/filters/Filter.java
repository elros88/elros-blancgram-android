package com.blanclink.blancgram.filters;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.widget.ImageView;

/**
 * Created by danort on 8/6/16.
 */
public abstract class Filter
{
    ImageView imageView;
    Bitmap source = null;
    Bitmap resultingBitmap = null;
    int[] originalPixels = null;

    public Filter(ImageView imageView)
    {
        this.imageView = imageView;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public void setImageView(Bitmap bitmap) {
        this.imageView.setImageBitmap(bitmap);
    }

    public void configureFilter()
    {
        if (imageView != null) {
            source = ((BitmapDrawable)imageView.getDrawable()).getBitmap();
        }
        if (source != null) {
            originalPixels = new int[source.getWidth() * source.getHeight()];
        }
    }

    public abstract Bitmap applyFilter();

    public int getAlphaFromPixel(int pixel) { return pixel >> 24 & 0xFF; }
    public int getRedFromPixel(int pixel) { return pixel >> 16 & 0xFF; }
    public int getGreenFromPixel(int pixel) { return pixel >> 8 & 0xFF; }
    public int getBlueFromPixel(int pixel) { return pixel & 0xFF; }
    public int createPixel(int alpha, int red, int green, int blue) {
        return (alpha << 24) + (red << 16) + (green << 8) + blue;
    }
}
