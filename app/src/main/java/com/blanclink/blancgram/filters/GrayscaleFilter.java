package com.blanclink.blancgram.filters;

import android.graphics.Bitmap;
import android.widget.ImageView;

/**
 * Created by danort on 8/6/16.
 */
public class GrayscaleFilter extends Filter
{

    public GrayscaleFilter(ImageView imageView)
    {
        super(imageView);
    }

    @Override
    public Bitmap applyFilter() {

        configureFilter();

        if (originalPixels != null) {
            source.getPixels(originalPixels, 0, source.getWidth(), 0, 0, source.getWidth(), source.getHeight());
            for (int i = 0; i < originalPixels.length; i++) {
                int average = ( getRedFromPixel(originalPixels[i]) +
                        getGreenFromPixel(originalPixels[i]) +
                        getBlueFromPixel(originalPixels[i])
                ) / 3;
                int newPixel = createPixel(getAlphaFromPixel(originalPixels[i]), average, average, average);
                originalPixels[i] = newPixel;
            }
            resultingBitmap =  Bitmap.createBitmap(originalPixels, source.getWidth(), source.getHeight(), Bitmap.Config.ARGB_8888);

        }
        return resultingBitmap;
    }
}
