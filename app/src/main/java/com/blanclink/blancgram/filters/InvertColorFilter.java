package com.blanclink.blancgram.filters;

import android.graphics.Bitmap;
import android.widget.ImageView;

/**
 * Created by danort on 8/7/16.
 */
public class InvertColorFilter
        extends Filter {

    public InvertColorFilter(ImageView imageView) {
        super(imageView);
    }

    @Override
    public Bitmap applyFilter() {

        configureFilter();

        if (originalPixels != null) {
            source.getPixels(originalPixels, 0, source.getWidth(), 0, 0, source.getWidth(), source.getHeight());
            for (int i = 0; i < originalPixels.length; i++) {
                int alpha = getAlphaFromPixel(originalPixels[i]);
                int red = getRedFromPixel(originalPixels[i]);
                int green = getGreenFromPixel(originalPixels[i]);
                int blue = getBlueFromPixel(originalPixels[i]);
                int newPixel = createPixel(alpha, (255 - red), (255 - green), (255 - blue));
                originalPixels[i] = newPixel;
            }
            resultingBitmap = Bitmap.createBitmap(originalPixels, source.getWidth(), source.getHeight(), Bitmap.Config.ARGB_8888);
        }
        return resultingBitmap;
    }
}
