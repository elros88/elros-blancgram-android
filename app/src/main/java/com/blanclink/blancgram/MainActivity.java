package com.blanclink.blancgram;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Debug;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import com.blanclink.blancgram.filters.Filter;
import com.blanclink.blancgram.filters.GrayscaleFilter;
import com.blanclink.blancgram.filters.InvertColorFilter;
import com.blanclink.blancgram.filters.SepiaFilter;

import com.blanclink.blancgram.imageContainer.ImageContainer;


public class MainActivity extends AppCompatActivity {

    ImageContainer image;
    ImageView imageView;
    ProgressDialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView = (ImageView) findViewById(R.id.imageView);

    }

    private void setDialog(String message)
    {
        dialog = new ProgressDialog(MainActivity.this);
        dialog.setCancelable(false);
        dialog.setTitle(R.string.dialogTitle);
        dialog.setMessage(message);
        dialog.show();
    }

    private void applyFilter(Filter filter)
    {
        FilterAsyncTask filterAsyncTask = new FilterAsyncTask(filter){
            @Override
            public void onDone(Bitmap bitmap)
            {
                filter.setImageView(bitmap);
                if (dialog.isShowing())
                {                    ;
                    dialog.dismiss();
                }
            }
        };
        filterAsyncTask.execute();
    }

    public void buttonClicked(View view)
    {
        Button btn = (Button)view;
        Filter filter;

        switch (btn.getText().toString())
        {
            case "Grayscale":
                setDialog(getResources().getString(R.string.grayScaleMessage));
                applyFilter(new GrayscaleFilter(imageView));
                break;
            case "Sepia":
                setDialog(getResources().getString(R.string.sepiaMessage));
                applyFilter(new SepiaFilter(imageView));
                break;
            case "Invert":
                setDialog(getResources().getString(R.string.invertMessage));
                applyFilter(new InvertColorFilter(imageView));
                break;
            default:
                break;
        }
    }

    public void saveImage(View view)
    {
        ImageView imageView = (ImageView) findViewById(R.id.imageView);
        image = new ImageContainer(this, imageView);
        image.saveImage();

    }
}
