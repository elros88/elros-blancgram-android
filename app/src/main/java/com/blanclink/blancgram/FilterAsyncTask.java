package com.blanclink.blancgram;

import android.graphics.Bitmap;

import com.blanclink.blancgram.filters.Filter;

/**
 * Created by danort on 8/6/16.
 */
public abstract class FilterAsyncTask extends GenericAsyncTask <Void, Void, Bitmap>
{
    Filter filter;
    public FilterAsyncTask(Filter filter)
    {
        this.filter = filter;
    }

    @Override
    protected Bitmap doInBackground(Void... params) {
        return filter.applyFilter();
    }

}
